from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages


# Create your views here.
def index(request):
	print ("#==> masuk index")
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('login:index'))
	else: 
		return render(request, 'login/session/login.html')

def set_data_for_session(res, request):
	response['author'] = request.session['user_login']
