from django.conf.urls import url
from .views import registrasiRelawan, registrasiDonatur, registrasiSponsor, indexHome, index_after, validate_email
#url for app
urlpatterns = [
    url(r'^$', indexHome, name='indexHome'),
    url(r'^home', indexHome, name='indexHome'),
    url(r'^regis_relawan', registrasiRelawan, name='registrasiRelawan'),
    url(r'^regis_donatur', registrasiDonatur, name='registrasiDonatur'),
    url(r'^regis_sponsor', registrasiSponsor, name='registrasiSponsor'),
    url(r'^after_regis', index_after, name='index_after'),
    url(r'^validate-email', validate_email, name='validate-email'),
    
]
