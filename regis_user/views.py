from django.shortcuts import render
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, JsonResponse
import psycopg2

# Create your views here.
def index(request):
	return render(request, 'regis_relawan.html')

def index1(request):
	return render(request, 'regis_donatur.html')

def index2(request):
	return render(request, 'regis_sponsor.html')

def indexHome(request):
	return render(request, 'home.html')

def index_after(request):
	return render(request, 'after_regis.html')

@csrf_exempt
def validate_email(request):
	if request.method=='POST':
		npm = request.POST[email]
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")
		cursor.execute('select email from SION.USER')
		email_org = cursor.fetchall()
		for e in email_org :
			if e[0] == npm:
				taken = True
				break
			else :
				taken = False

	data = {
		'is_taken': taken
	}
	return JsonResponse(data)


@csrf_exempt
def registrasiRelawan(request):
	#print("mask form")
	email = request.POST['email']
	no_hp = request.POST['nomor-hp']
	password = request.POST['password']
	tanggal_lahir = request.POST['tanggal-lahir']
	nama = request.POST['nama-org']
	provinsi = request.POST['provinsi']
	kabupaten = request.POST['kabupaten']
	kecamatan = request.POST['kecamatan']
	alamat_lengkap = request.POST['alamat-lengkap']
	kode_pos = request.POST['kode-pos']

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")
		cursor.execute("INSERT INTO SION.USER (email,password,nama,alamat_lengkap) VALUES (%s, %s, %s, %s)", [email,password,nama,alamat_lengkap])
		cursor.execute("INSERT INTO relawan (email,no_hp,tanggal_lahir) VALUES (%s, %s, %s)", [email,no_hp,tanggal_lahir])
		
		return HttpResponseRedirect("/regis_relawan")

@csrf_exempt
def registrasiDonatur(request):
	#print("mask form")
	email = request.POST['email']
	nama = request.POST['nama-org']
	password = request.POST['password']
	provinsi = request.POST['provinsi']
	kabupaten = request.POST['kabupaten']
	kecamatan = request.POST['kecamatan']
	alamat_lengkap = request.POST['alamat-lengkap']
	kode_pos = request.POST['kode-pos']

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")
		cursor.execute("INSERT INTO donatur (email,saldo) VALUES (%s, %s)", [email,saldo])
		cursor.execute("INSERT INTO SION.USER (email,password,nama,alamat_lengkap) VALUES (%s, %s, %s, %s)", [email,password,nama,alamat_lengkap])
		return HttpResponseRedirect("/regis_donatur")
@csrf_exempt
def registrasiSponsor(request):
	#print("mask form")
	email = request.POST['email']
	logo_sponsor = request.Post['logo']
	password = request.POST['password']
	nama = request.POST['nama-org']
	provinsi = request.POST['provinsi']
	kabupaten = request.POST['kabupaten']
	kecamatan = request.POST['kecamatan']
	alamat_lengkap = request.POST['alamat-lengkap']
	kode_pos = request.POST['kode-pos']

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")
		cursor.execute("INSERT INTO sponsor (email,logo_sponsor) VALUES (%s, %s)", [email,logo_sponsor])
		cursor.execute("INSERT INTO SION.USER (email,password,nama,alamat_lengkap) VALUES (%s, %s, %s, %s)", [email,password,nama,alamat_lengkap])
		return HttpResponseRedirect("/regis_sponsor")
