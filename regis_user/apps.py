from django.apps import AppConfig


class RegisUserConfig(AppConfig):
    name = 'regis_user'
