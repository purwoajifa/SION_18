from django.shortcuts import render
from django.db import connection

# Create your views here.
response = {}
def index(request):
	response['email'] = "purwo@gmail.com"
	cursor = connection.cursor()
	cursor.execute("SET search_path to SION;")

	cursor.execute("SELECT * FROM SPONSOR WHERE email = '"+response['email'] + "'")
	arrSponsor = cursor.fetchall()

	cursor.execute("SELECT * FROM DONATUR WHERE email = '"+response['email'] + "'")
	arrDonatur = cursor.fetchall()

	cursor.execute("SELECT * FROM RELAWAN WHERE email = '"+response['email'] + "'")
	arrRelawan = cursor.fetchall()

	if len(arrSponsor) != 0:
		email = "randa@gmail.com"
		cursor.execute("SELECT nama,email,alamat_lengkap, password FROM sion.user WHERE email = '"+email+"'")
		response['profil_user'] = cursor.fetchone()
		cursor.execute("SELECT logo_sponsor FROM SPONSOR WHERE email = '"+email+"'")
		response['logo_sponsor'] = cursor.fetchone()
		html = 'profil_pribadi/profil_sponsor.html'
		return render(request, html, response)

	elif len(arrDonatur) != 0:
		email = "afwan@gmail.com"
		cursor.execute("SELECT nama,email,alamat_lengkap, password FROM sion.user WHERE email = '"+email+"'")
		response['profil_user'] = cursor.fetchone()
		html = 'profil_pribadi/profil_donatur.html'
		return render(request, html, response)

	elif len(arrRelawan) != 0:
		email = "purwo@gmail.com"
		cursor.execute("SELECT nama,email,alamat_lengkap,password FROM sion.user WHERE email = '"+email+"'")
		response['profil_user'] = cursor.fetchone()
		cursor.execute("SELECT no_hp, tanggal_lahir FROM RELAWAN WHERE email = '"+email+"'")
		response['profil_relawan'] = cursor.fetchone()
		cursor.execute("SELECT keahlian FROM keahlian_relawan WHERE email = '"+email+"'")
		response['keahlian_relawan'] = cursor.fetchone()
		html = 'profil_pribadi/profil_relawan.html'
		return render(request, html, response)    
	return render(request, 'profil_pribadi/profil_pribadi.html')

def index_relawan(request):
	return render(request, 'profil_pribadi/profil_relawan.html')

def index_donatur(request):
	return render(request, 'profil_pribadi/profil_donatur.html')

def index_sponsor(request):
	return render(request, 'profil_pribadi/profil_sponsor.html')