from django.conf.urls import url
from .views import index, add_session_organisasi,form_donasi_organisasi, donasi, jumlah_dana
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_session_organisasi',add_session_organisasi,name='add_session_organisasi'),
    url(r'^form_donasi',form_donasi_organisasi,name='form_donasi_organisasi'),
    url(r'^donasi',donasi,name='donasi'),
    url(r'^jumlah_dana',jumlah_dana,name='jumlah_dana'),
]
