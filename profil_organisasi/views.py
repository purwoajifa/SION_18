from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
import psycopg2
import json
import datetime

# Create your views here.
response={}

def index(request):
	cursor = connection.cursor()
	cursor.execute("SET SEARCH_PATH TO SION;")
	query = "select email_organisasi,website, nama, provinsi,kabupaten_kota, kecamatan,kelurahan,kode_pos from organisasi"
	cursor.execute(query)
	result = cursor.fetchall()
	response['organisasi'] = result
	if 'organisasi' in request.session.keys():
		response['det_organisasi'] = request.session['organisasi']
	# jika tidak ditambahkan else, cache akan tetap menyimpan data
	# sebelumnya yang ada pada response, sehingga data tidak up-to-date
	else:
		response['det_organisasi'] = []

	return render(request, 'profil_organisasi.html', response)

def add_session_organisasi(request, id):
    ssn_key = request.session.keys()
    if not 'organisasi' in ssn_key:
        request.session['organisasi'] = [id]
    else:
        organisasi = request.session['organisasi']
        if id not in organisasi:
            organisasi.append(id)
            request.session['organisasi'] = organisasi

    return HttpResponseRedirect(reverse('profil_organisasi:organisasi_detail'))
def organisasi_detail(request):
	organisasi = request.session['organisasi']
	response['detail'] = organisasi
	# cursor = connection.cursor()
	# query = "select * from organisasi "
	# cursor.execute(query)
	# result1 = cursor.fetchall()
	# response['tentang'] = result1


	# query = "select * from pengurus_organisasi PO, user u where"
	# cursor.execute(query)
	# result2 = cursor.fetchall()
	# response['pengurus'] = result2


	# query = "select * from donatur_organisasi DI, donatur D, user U"
	# cursor.execute(query)
	# result3 = cursor.fetchall()
	# response['donatur'] = result3


	# query = "select * from sponsor_organisasi SO, sponsor S, user U"
	# cursor.execute(query)
	# result4 = cursor.fetchall()
	# response['sponsor'] = result4

	# query = "select * from organisasi O, tujuan_organisasi TO"
	# cursor.execute(query)
	# result5 = cursor.fetchall()
	# response['tujuan'] = result5

	return render(request,'organisasi_detail.html',response)

def form_donasi_organisasi(request):
	# cursor = connection.cursor()
	# query = "select * from organisasi"
	# cursor.execute(query)
	# result = cursor.fetchall()
	# response['donasi'] = result
	return render(request,'form_donasi.html')

@csrf_exempt
def donasi(request):
	print("mask form")
	organs = request.POST['organisasi']
	jumlah_dana = request.POST['jumlah-dana']
	time = datetime.datetime.now()
	time_real = '{:%Y-%m-%d %H:%M:%S}'.format(time)

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION")
		cursor.execute("INSERT INTO  donatur_organisasi(donatur,organisasi,tanggal,nominal) VALUES (%s,%s,%s,%s)",[organs,time_real,jumlah_dana])
		cursor.execute("UPDATE donatur SET saldo= '"+jumlah_dana+"'")
		return HttpResponseRedirect("/form_donasi")

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION")
		cursor.execute("INSERT INTO  sponsor_organisasi(sponsor,organisasi,tanggal,nominal) VALUES (%s,%s,%s,%s)",[organs,time_real,jumlah_dana])
		return HttpResponseRedirect("/form_donasi")

@csrf_exempt
def jumlah_dana(request):
	if request.method=='POST':
		jumlah = request.POST[jumlah_dana]
		if jumlah < 2000000:
				good = True
		else :
			bad = False

	data = {
		'is_good': good
	}
	return JsonResponse(data)
