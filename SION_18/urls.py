"""SION_18 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import regis_organisasi.urls as regis_organisasi
import login.urls as login
import profil_organisasi.urls as profil_organisasi
import regis_user.urls as regis_user
import kegiatan.urls as kegiatan
import profil_pribadi.urls as profil_pribadi


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^regis_organisasi/', include(regis_organisasi,namespace='regis_organisasi')),
    url(r'^login/', include(login,namespace='login')),
    url(r'^$',RedirectView.as_view(url='login/',permanent=True), name='index'),
    url(r'^profil_organisasi/', include(profil_organisasi,namespace='profil_organisasi')),
    url(r'^regis_user/', include(regis_user,namespace='regis_user')),
    url(r'^kegiatan/',include(kegiatan,namespace='kegiatan')),
    url(r'^profil_pribadi/',include(profil_pribadi,namespace='profil_pribadi')),
]

