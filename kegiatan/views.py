from django.shortcuts import render

# Create your views here.
def index(request) :
	return render(request,'daftar_organisasi.html')

def daftar_kegiatan(request):
	return render(request,'daftar_kegiatan.html')

def form_buat_kegiatan(request):
	return render(request,'form_buat_kegiatan.html')

def form_donasi(request):
	return render(request,'form_donasi.html')