from django.conf.urls import url
from .views import index, daftar_kegiatan,form_donasi,form_buat_kegiatan
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftar-kegiatan',daftar_kegiatan,name='daftar-kegiatan'),
    url(r'^form-donasi',form_donasi,name='form-donasi'),
    url(r'^form-buat-kegiatan',form_buat_kegiatan,name='form-buat-kegiatan'),
]
