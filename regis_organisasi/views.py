from django.shortcuts import render
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages
import psycopg2
# Create your views here.

response = {}
def index(request):
	return render(request, 'regis_organisasi.html')

def index_after(request):
	return render(request, 'after_regis.html')

@csrf_exempt
def validate_email(request):
	if request.method=='POST':
		npm = request.POST[email]
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")
		cursor.execute('select email_organisasi from organisasi')
		email_org = cursor.fetchall()
		for e in email_org :
			if e[0] == npm:
				taken = True
				break
			else :
				taken = False

	data = {
		'is_taken': taken
	}
	return JsonResponse(data)

# def __init__(self):
#                 try:
#                         self.connection = psycopg2.connect(
#                                 dbname="purwoaji", user="purwoaji", password="paris1732011")
#                         self.connection.autocommit = True
#                         self.cursor = self.connection.cursor()
#                         print("test")
#                 except:
#                         print("Cannot connect to database")

# def registrasi(request):
# 	if (request.method=='POST'):
# 		connection = psycopg2.connect('dbname=purwoaji', 'user=purwoaji')
# 		mark = connection.cursor()
# 		statement = 'INSERT INTO ' + 'organisasi' + ' VALUES (' + request.POST['email'] + ',' + request.POST['web'] + ',' + request.POST['nama-org'] + ',' + request.POST['provinsi'] + ',' + request.POST['kabupaten'] + ',' + request.POST['kecamatan'] + ',' + request.POST['kelurahan'] + ',' + request.POST['kode-pos'] + ',' + 'Tidak Aktif' + ')'
# 		mark.execute(statement)
# 		connection.commit() 
# 		return render(request, 'after_regis.html')
@csrf_exempt
def registrasi(request):
	print("mask form")
	email = request.POST['email']
	web = request.POST['web']
	nama_organisasi = request.POST['nama-org']
	provinsi = request.POST['provinsi']
	kabupaten = request.POST['kabupaten']
	kecamatan = request.POST['kecamatan']
	alamat_lengkap = request.POST['alamat-lengkap']
	kode_pos = request.POST['kode-pos']
	email_pengurus = request.POST['email-pgr']

	if request.method=='POST' :
		cursor = connection.cursor()
		cursor.execute("SET search_path to SION;")

		cursor.execute('select email_organisasi from organisasi')
		email_org = cursor.fetchall()
		taken = True
		for e in email_org :
			if e[0] == email:
				taken = False
				break
			else :
				taken = True

		cursor.execute('select nama from organisasi')
		nama_org = cursor.fetchall()
		point = True
		for j in nama_org :
			if j[0] == nama_org:
				point = False
				break
			else :
				point = True

		cursor.execute('select email from sion.user')
		email_pgr = cursor.fetchall()
		pointer = True
		for i in email_pgr:
			if i[0] == email_pengurus:
				pointer = True
				break
			else :
				pointer = False


		if taken and point and pointer :
			print("HOLAAAAAA")
			
			cursor.execute("SELECT count(*) FROM organisasi")
			response['nomor_reg'] = cursor.fetchone()[0]
			print(response['nomor_reg'])
			cursor.execute("SELECT password FROM SION.user WHERE email='" +email_pengurus+ "'")
			response['password'] = cursor.fetchone()[0]
			print(response['password'])
			response['email_pengurus'] = email_pengurus
			print(response['email_pengurus'])
			cursor.execute("INSERT INTO organisasi (email_organisasi,website,nama,provinsi,kabupaten_kota,kecamatan,kelurahan,kode_pos,status_verifikasi) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, 'Aktif')", [email,web,nama_organisasi,provinsi,kabupaten,kecamatan,alamat_lengkap,kode_pos])
			cursor.execute("INSERT INTO pengurus_organisasi (email, organisasi) VALUES (%s,%s)", [email_pengurus,email])
			html = 'after_regis.html'
			return render(request, html, response)
		else :
			print("JAAKAKAKAK")
			messages.error(request, "Input yang dimasukkan sudah terdaftar")
		return HttpResponseRedirect("/regis_organisasi")

