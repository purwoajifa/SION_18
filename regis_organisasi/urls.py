from django.conf.urls import url
from .views import index, index_after, registrasi, validate_email
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^after_regis', index_after, name='index_after'),
    url(r'^register', registrasi, name='register'),
    url(r'^validate-email', validate_email, name='validate-email'),
]
